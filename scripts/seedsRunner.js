const logger = require('@pubsweet/logger')
const {
  createAdminUser,
  createApplicationParams,
  createBookCollection,
  createGlobalTeams,
} = require('./seeds')
const { createTemplates } = require('editoria-templates')

const runner = async () => {
  try {
    await createAdminUser({
      username: 'admin',
      password: 'password',
      email: 'admin@example.com',
      admin: true,
    })
    await createApplicationParams()
    await createBookCollection()
    await createGlobalTeams()
    await createTemplates()
  } catch (e) {
    logger.error(e.message)
  }
}

runner()
