const { user1 } = require('../support/credentials')

const checkFormError = message => {
  cy.get('div[role=alert]').each($el => {
    if ($el[0].innerHTML !== '') {
      cy.wrap($el)
        .contains(message)
        .should('be.visible')
    }
  })
}

describe('Signup page', () => {
  beforeEach(() => {
    cy.exec('node ./scripts/truncateDB.js')
  })

  it('loads signup page', () => {
    cy.visit('/signup')
    cy.contains('Sign up')
  })

  it('successfully creates user', () => {
    cy.visit('/signup')

    const { givenName, surname, email, username, password } = user1

    cy.get('input[name="givenName"]').type(givenName)
    cy.get('input[name="surname"]').type(surname)
    cy.get('input[name="username"]').type(username)
    cy.get('input[name="email"]').type(email)
    cy.get('input[name="password"]').type(password)
    cy.get('button[type="submit"]').click()
    cy.url().should('include', '/login')

    cy.get('input[name="username"]').type(username)
    cy.get('input[name="password"]').type(password)
    cy.get('button[type="submit"]').click()
    cy.url().should('include', '/books')
  })

  it('fails if required fields not filled in', () => {
    const { givenName, surname } = user1

    cy.visit('/signup')

    cy.get('input[name="username"]')
      .focus()
      .blur()
    cy.get('input[name="email"]')
      .focus()
      .blur()
    cy.get('input[name="password"]')
      .focus()
      .blur()
    cy.get('button[type="submit"]').should('be.disabled')
    checkFormError('Required')

    cy.get('input[name="givenName"]').type(givenName)
    cy.get('input[name="surname"]')
      .focus()
      .blur()
    checkFormError('Required')

    cy.get('input[name="surname"]').type(surname)
    cy.get('input[name="givenName"]')
      .clear()
      .blur()
    checkFormError('Required')
  })
})
